\graphicspath{ {./images/} }
\section{Описание метода}
Метод NSG дополняет FDTD для получения лучшего разрешения электромагнитного поля в некоторой части расчётной области. Он вносит дополнительные операции к одному обновлению поля \(\mathbf{B}\) и одному обновлению поля \(\mathbf{E}\) в рамках одной итерации. Во-первых, метод NSG также обновляет электрическое и магнитное поля в подсетке. Во-вторых, он включает прямое и обратное связывания. Далее размещение новых операций в цикле FDTD и механизм проведения связываний рассматриваются более подробно с иллюстрациями для множителя разбиения 3. Рассмотрение пространственных аспектов (границы обновления FDTD, механизм связывания) производится для метода с одинаковыми шагами по времени в основной сетке и подсетке, после этого рассматривается случай с разбиением временного шага в подсетке.

\subsection{Место операций метода в цикле FDTD}\label{sec:nsg-order}
Операции метода вставляются в цикл FDTD таким образом, чтобы значения компонент одинаковых полей в основной сетке и в подсетке соответствовали одному и тому же моменту времени. Цикл FDTD вместе с операциями метода выглядит следующим образом:
\begin{enumerate}
    \item Обновление поля \(\mathbf{E}\) на основной сетке.
    \item Обновление поля \(\mathbf{E}\) в подсетке.
    \item Прямое связывание.
    \item Обновление поля \(\mathbf{B}\) на основной сетке.
    \item Обновление поля \(\mathbf{B}\) в подсетке.
    \item Обратное связывание.
\end{enumerate}

\subsection{Строение подсетки и основной сетки}
Неравномерная подсетка является отличительной особенностью метода NSG. Она строится следующим образом. Берётся блок ячеек Йи, имеющий форму прямоугольного параллелепипеда размером \(m\times n\times k\) ячеек. Пусть также ячейка на одном угле этого блока имеет индексы \((0, 0, 0)\), тогда противоположная ей ячейка имеет индексы \((m-1,n-1,k-1)\). Пусть \(p \in \mathbb{N}\), нечётное --- множитель разбиения по пространственным координатам. Тогда подсетка получается разделением ячеек с первой компонентой индекса в диапазоне \(2,...,m-3\) на \(p\) равных частей вдоль оси \(x\), ячеек со второй компонентой индекса в диапазоне \(2,...,n-3\) --- вдоль оси \(y\), и ячеек с третьей компонентой индекса в диапазоне \(2,...,k-3\) --- вдоль оси \(z\). На рис.~\ref{fig:two-axis-division} изображена грань блока ячеек Йи для случая \(m=6\), \(n=6\), \(p=3\). Сплошные линии отвечают границам ячеек Йи основной сетки, пунктирные --- линиям разбиения.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.35\textwidth]{two-axis-division}
    \caption{Грань блока с подсеткой}
    \label{fig:two-axis-division}
\end{figure}

В результате ячейки блока с индексами от \((2,2,2)\) до \((m-3,n-3,k-3)\) разбиваются на \(p^3\) частей. Ячейки, расположенные на гранях исходного блока (например, ячейки с индексами от \((0,2,2)\) до \((0,n-3,k-3)\), разбиваются на \(p^2\) частей. Ячейки, расположенные на рёбрах исходного блока, разбиваются на \(p\) частей, а \(64\) угловых ячейки не разбиваются вовсе.

Основная сетка в той области, где помещается подсетка, имеет полость. Для рассматриваемой подсетки эта полость имеет размер \((m-4,n-4,k-4)\) ячеек основной сетки, то есть в ней помещается лишь та часть подсетки, ячейки которой разбиваются по всем трём осям. Оставшаяся часть подсетки накладывается на ячейки основной сетки, соседствующие с полостью. На рис.~\ref{fig:subgrid-embedding} изображён пример внедрения двумерной подсетки размера \(6 \times 6\) в полость основной сетки.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{subgrid-embedding}
    \caption{Двумерный пример наложения подсетки на полость в основной сетки}
    \label{fig:subgrid-embedding}
\end{figure}

\subsection{Границы обновления полей с помощью FDTD}
Границы областей обновления полей на основной сетке и на подсетке диктуются расположением компонент полей, участвующих в связываниях. Необходимо, чтобы на основной сетке с помощью FDTD обновлялись компоненты поля \(\mathbf{E}\), которые участвуют в прямом связывании, а на подсетке --- компоненты поля \(\mathbf{B}\), участвующие в обратном связывании. Кроме этого, границы обновления должны быть симметричны относительно центра подсетки по каждой оси. Рис.~\ref{fig:e-mg-update-borders} и~\ref{fig:b-mg-update-borders} иллюстрируют расположение компонент полей в сечении \(z=\mathrm{const}\) основной сетки, находящихся вблизи интерфейса и обновляемых FDTD. Серым цветом закрашены ячейки, в которых есть компоненты, обновляемые FDTD. Точками отмечены компоненты \(E_x\) и \(B_x\), крестиками --- \(E_y\) и \(B_y\), окружностями --- \(E_z\) и \(B_z\). На двух других двумерных сечениях (\(x=\mathrm{const}\) и \(y=\mathrm{const}\)) соответствующие компоненты полей, обновляемые в основной сетке, находятся в тех же точках.

\begin{figure}[H]
    \centering
	\includegraphics[width=0.45\textwidth]{e-mg-update-borders}
    \caption{Компоненты поля \(\mathbf{E}\), обновляемые в основной сетке}
    \label{fig:e-mg-update-borders}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{b-mg-update-borders}
    \caption{Компоненты поля \(\mathbf{B}\), обновляемые в основной сетке}
    \label{fig:b-mg-update-borders}
\end{figure}

На рис.~\ref{fig:e-sg-update-borders} и~\ref{fig:b-sg-update-borders} изображены компоненты полей, обновляемые при помощи FDTD в подсетке. Эти рисунки также изображают сечение \(z=\mathrm{const}\).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{e-sg-update-borders}
    \caption{Компоненты поля \(\mathbf{E}\), обновляемые в подсетке}
    \label{fig:e-sg-update-borders}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{b-sg-update-borders}
    \caption{Компоненты поля \(\mathbf{B}\), обновляемые в подсетке}
    \label{fig:b-sg-update-borders}
\end{figure}

Поскольку подсетка является неравномерной сеткой, формулы обновления метода FDTD на ней отличаются от формул (\ref{eq:fdtd-first}-\ref{eq:fdtd-last}). Вместо общих размеров ячеек \(\Delta x\), \(\Delta y\), \(\Delta z\) каждая ячейка имеет свой размер: \(\Delta x_{i,j,k}\), \(\Delta y_{i,j,k}\), \(\Delta z_{i,j,k}\). С учётом этого формулы обновления записываются следующим образом:
\begingroup
\allowdisplaybreaks[1]
\begin{align}
    \left.B_x^{n+\frac12}\right|_{i+\frac12,j,k}=\left.B_x^{n-\frac12}\right|_{i+\frac12,j,k}+c\Delta t&\left(\frac{\left.E_y^n\right|_{i+\frac12,j,k+\frac12}-\left.E_y^n\right|_{i+\frac12,j,k-\frac12}}{\frac12\left(\Delta z_{i,j,k}+\Delta z_{i,j,k-1}\right)}-\right.\notag \\
	&\qquad\left.-\frac{\left.E_z^n\right|_{i+\frac12,j+\frac12,k}-\left.E_z^n\right|_{i+\frac12,j-\frac12,k}}{\frac12\left(\Delta y_{i,j,k}+\Delta y_{i,j-1,k}\right)}\right),\label{eq:fdtd-nonuniform-first}\displaybreak[4]\\
    \left.B_y^{n+\frac12}\right|_{i,j+\frac12,k}=\left.B_y^{n-\frac12}\right|_{i,j+\frac12,k}+c\Delta t&\left(\frac{\left.E_z^n\right|_{i+\frac12,j+\frac12,k}-\left.E_z^n\right|_{i-\frac12,j+\frac12,k}}{\frac12\left(\Delta x_{i,j,k}+\Delta x_{i-1,j,k}\right)}-\right.\notag \\
	&\qquad\left.-\frac{\left.E_x^n\right|_{i,j+\frac12,k+\frac12}-\left.E_x^n\right|_{i,j+\frac12,k-\frac12}}{\frac12\left(\Delta z_{i,j,k}+\Delta z_{i,j,k-1}\right)}\right),\\
    \left.B_z^{n+\frac12}\right|_{i,j,k+\frac12}=\left.B_z^{n-\frac12}\right|_{i,j,k+\frac12}+c\Delta t&\left(\frac{\left.E_x^n\right|_{i,j+\frac12,k+\frac12}-\left.E_x^n\right|_{i,j-\frac12,k+\frac12}}{\frac12\left(\Delta y_{i,j,k}+\Delta y_{i,j-1,k}\right)}-\right.\notag \\
	&\qquad\left.-\frac{\left.E_y^n\right|_{i+\frac12,j,k+\frac12}-\left.E_y^n\right|_{i-\frac12,j,k+\frac12}}{\frac12\left(\Delta x_{i,j,k}+\Delta x_{i-1,j,k}\right)}\right),\\
    \left.E_x^{n+1}\right|_{i,j+\frac12,k+\frac12}=\left.E_x^n\right|_{i,j+\frac12,k+\frac12}+c\Delta t&\left(\frac{\left.B_z^{n+\frac12}\right|_{i,j+1,k+\frac12}-\left.B_z^{n+\frac12}\right|_{i,j,k+\frac12}}{\Delta y_{i,j,k}}-\right.\notag \\
	&\qquad\left.-\frac{\left.B_y^{n+\frac12}\right|_{i,j+\frac12,k+1}-\left.B_y^{n+\frac12}\right|_{i,j+\frac12,k}}{\Delta z_{i,j,k}}\right),\\
    \left.E_y^{n+1}\right|_{i+\frac12,j,k+\frac12}=\left.E_y^n\right|_{i+\frac12,j,k+\frac12}+c\Delta t&\left(\frac{\left.B_x^{n+\frac12}\right|_{i+\frac12,j,k+1}-\left.B_x^{n+\frac12}\right|_{i+\frac12,j,k}}{\Delta z_{i,j,k}}-\right.\notag \\
	&\qquad\left.-\frac{\left.B_z^{n+\frac12}\right|_{i+1,j,k+\frac12}-\left.B_z^{n+\frac12}\right|_{i,j,k+\frac12}}{\Delta x_{i,j,k}}\right),\\
    \left.E_z^{n+1}\right|_{i+\frac12,j+\frac12,k}=\left.E_z^n\right|_{i+\frac12,j+\frac12,k}+c\Delta t&\left(\frac{\left.B_y^{n+\frac12}\right|_{i+1,j+\frac12,k}-\left.B_y^{n+\frac12}\right|_{i,j+\frac12,k}}{\Delta x_{i,j,k}}-\right.\notag \\
	&\qquad\left.-\frac{\left.B_x^{n+\frac12}\right|_{i+\frac12,j+1,k}-\left.B_x^{n+\frac12}\right|_{i+\frac12,j,k}}{\Delta y_{i,j,k}}\right).\label{eq:fdtd-nonuniform-last}
\end{align}
\endgroup

\subsection{Связывания}
Вспомним, что интерфейс есть множество граничных ячеек подсетки. Его можно представить в виде объединения трёх пар параллельных слоёв --- групп ячеек с общим индексом по одной из координат. Будем называть эти слои ``гранями'' подсетки. Оба связывания производятся одинаково в параллельных ``гранях''. Рассмотрим процедуры прямого и обратного связываний на примере одной из ``граней'', параллельных плоскости \(xOy\).

\subsubsection{Прямое связывание}\label{subsec:forward-coupling}
Прямое связывание выполняется с целью получения недостающих значений поля \(\mathbf{E}\) в подсетке. На рис.~\ref{fig:forward-coupling-region} серым цветом выделены ячейки, в которых находятся компоненты поля \(\mathbf{E}\), участвующие в прямом связывании:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{forward-coupling-region}
    \caption{Ячейки, содержащие компоненты поля, участвующие в прямом связывании}
    \label{fig:forward-coupling-region}
\end{figure}

В выделенных слоях ячеек в прямом связывании участвуют компоненты \(E_x\) и \(E_y\). Ячейки из этих слоёв не подразбиты по оси \(z\), поэтому в дальнейшем они будут изображаться в плоскости \(xOy\). На рис.~\ref{fig:forward-coupling-used-values} изображены компоненты, участвующие в прямом связывании:
\begin{itemize}
    \item точками отмечены компоненты \(E_x\) на подсетке;
    \item окружностями вокруг точек --- компоненты \(E_x\) на основной сетке, совмещённые с компонентами на подсетке;
    \item крестиками --- компоненты \(E_y\) на подсетке;
    \item окружностями вокруг крестиков --- компоненты \(E_y\) на основной сетке, совмещённые с компонентами на подсетке.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{forward-coupling-used-values}
    \caption{Компоненты поля \(\mathbf{E}\), участвующие в прямом связывании}
    \label{fig:forward-coupling-used-values}
\end{figure}

Непосредственно механизм прямого связывания есть, в общем случае, интерполяция. Для \(E_x\) используется линейная интерполяция в направлении, параллельном оси \(x\), и константная --- в направлении, параллельном оси \(y\). В интерполяции могут участвовать одно или два значения поля \(\mathbf{E}\) из основной сетки. Рассмотрим пример, представленный на рис.~\ref{fig:forward-coupling-ex-example}. Заглавными буквами обозначены значения поля с основной сетки, строчными --- с подсетки. На нём отмечены три различных ситуации, возникающих при прямом связывании:
\begin{itemize}
    \item \(e_x^1\) получается без интерполяции, копированием значения совмещённой компоненты из основной сетки: \(e_x^1=E_x^1\);
    \item \(e_x^2\), \(e_x^3\), \(e_x^4\) получаются константной интерполяцией --- копированием значения из соответствующей ячейки основной сетки: \(e_x^2=e_x^3=e_x^4=E_x^2\);
    \item \(e_x^i,\;i=\overline{5,10}\) получаются линейной интерполяцией \(E_x^3\) и \(E_x^4\):
\[
\begin{matrix}
    e_x^5=e_x^6=e_x^7=\frac23 E_x^3 + \frac13 E_x^4,\\
    e_x^8=e_x^9=e_x^{10}=\frac13 E_x^3 + \frac23 E_x^4.
\end{matrix}
\]
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.55\textwidth]{forward-coupling-ex-example}
    \caption{Примеры прямого связывания с компонентой \(E_x\)}
    \label{fig:forward-coupling-ex-example}
\end{figure}

По аналогичным правилам происходит прямое связывание компонент \(E_y\): линейная интерполяция в направлении, параллельном оси \(y\) и константная --- в направлении, параллельном оси \(x\). Рис.~\ref{fig:forward-coupling-ey-example} содержит пример, для которого верны равенства, указанные выше.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.55\textwidth]{forward-coupling-ey-example}
    \caption{Примеры прямого связывания с компонентой \(E_y\)}
    \label{fig:forward-coupling-ey-example}
\end{figure}

Приведём формулы, описывающие прямое связывание на рассматриваемой грани. Будем обозначать индексы в основной сетке заглавными буквами, в подсетке --- строчными. Пусть ячейки подсетки имеют индексы от \(\left(0,0,0\right)\) до \(\left(i_f,j_f,k_f\right)\). Пусть ячейка подсетки с индексами \(\left(0,0,0\right)\) совпадает с ячейкой основной сетки \(\left(I_s,J_s,K_s\right)\), ячейка подсетки с индексами \(\left(i_f,j_f,k_f\right)\) совпадает с \(\left(I_f,J_f,K_f\right)\). Предполагая, что \(a\) и \(A\) есть пара индексов по одной координатной оси на подсетке и основной сетке соответственно, 	преобразование индекса на основной сетке в индекс на подсетке осуществляется по следующей формуле:
\begin{equation}\label{eq:sg-to-mg-index}
	A=
	\begin{cases}
		A_s+a & \text{, } a\in\left\{0,1\right\} \\
		A_s+2+\left\lfloor \frac{a-2}{p}\right\rfloor & \text{, } 2\leqslant a\leqslant a_f-2\\
		A_f-\left(a_f-a\right) & \text{, } a\in\left\{a_f-1,a_f\right\}
	\end{cases},
\end{equation}
Пусть коэффициент интерполяции \(\gamma_a\) по оси, соответствующей индексу \(a\), вычисляется по формуле:
\[
	\gamma_a=
	\begin{cases}
		\frac{\left(a-2\right) \bmod p}{p} & \text{, } 2\leqslant a\leqslant a_f-2\\
		0 & \text{, } \text{иначе}
	\end{cases}
\]

Тогда операцию прямого связывания на ``гранях'', параллельных плоскости \(z=\mathrm{const}\) можно записать в виде:
\begin{equation}\label{eq:front-ex-forward-coupling}
	\left.e_x\right|_{i,j+\frac12,\frac12} =
	\begin{cases}
		\left(1-\gamma_i\right)\left.E_x\right|_{I,J+\frac12,K_s+\frac12}+\gamma_i\left.E_x\right|_{I+1,J+\frac12,K_s+\frac12} & \text{, } 2\leqslant i\leqslant i_f-2 \\
		\left.E_x\right|_{I,J+\frac12,K_s+\frac12} & \text{, } \text{иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:front-ey-forward-coupling}
	\left.e_y\right|_{i+\frac12,j,\frac12}=
	\begin{cases}
		\left(1-\gamma_j\right)\left.E_y\right|_{I+\frac12,J,K_s+\frac12}+\gamma_j\left.E_y\right|_{I+\frac12,J+1,K_s+\frac12} & \text{, } 2\leqslant j\leqslant j_f-2 \\
		\left.E_y\right|_{I+\frac12,J,K_s+\frac12} & \text{, } \text{иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:back-ex-forward-coupling}
	\left.e_x\right|_{i,j+\frac12,k_f+\frac12} =
	\begin{cases}
		\left(1-\gamma_i\right)\left.E_x\right|_{I,J+\frac12,K_f+\frac12}+\gamma_i\left.E_x\right|_{I+1,J+\frac12,K_f+\frac12} & \text{, } 2\leqslant i\leqslant i_f-2 \\
		\left.E_x\right|_{I,J+\frac12,K_f+\frac12} & \text{, } \text{иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:back-ey-forward-coupling}
	\left.e_y\right|_{i+\frac12,j,k_f+\frac12}=
	\begin{cases}
		\left(1-\gamma_j\right)\left.E_y\right|_{I+\frac12,J,K_f+\frac12}+\gamma_j\left.E_y\right|_{I+\frac12,J+1,K_f+\frac12} & \text{, } 2\leqslant j\leqslant j_f-2 \\
		\left.E_y\right|_{I+\frac12,J,K_f+\frac12} & \text{, } \text{иначе}
	\end{cases}
\end{equation}

В формулах~\ref{eq:front-ex-forward-coupling} и \ref{eq:back-ex-forward-coupling} \(i=\overline{1..i_f}\), \(j=\overline{0..j_f}\), в формулах~\ref{eq:front-ey-forward-coupling} и \ref{eq:back-ey-forward-coupling} \(i=\overline{0..i_f}\), \(j=\overline{1..j_f}\). Формулы прямого связывания для других граней интерфейса записываются аналогичным образом.

\subsubsection{Обратное связывание}\label{subsec:backward-coupling}
Обратное связывание выполняется для того, чтобы перенести обновлённые значения поля \(\mathbf{B}\) из подсетки на интерфейс в основной сетке. На рис.~\ref{fig:backward-coupling-region} серым цветом закрашены ячейки, в которых находятся компоненты поля \(\mathbf{B}\), участвующие в обратном связывании:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{backward-coupling-region}
    \caption{Ячейки, содержащие компоненты поля, участвующие в обратном связывании}
    \label{fig:backward-coupling-region}
\end{figure}

В выделенных слоях ячеек в обратном связывании участвуют компоненты \(B_x\) и \(B_y\). Так как эти компоненты поля \(\mathbf{B}\) располагаются на гранях ячеек \(z=\mathrm{const}\), их можно изображать в плоскости \(xOy\). На рис.~\ref{fig:backward-coupling-used-values} изображены компоненты, участвующие в обратном связывании:
\begin{itemize}
    \item точками отмечены компоненты \(B_x\) на подсетке;
    \item точками внутри окружности --- компоненты \(B_x\) на основной сетке, совмещённые с компонентами на подсетке;
    \item крестиками --- компоненты \(B_y\) на подсетке;
    \item крестиками внутри окружности --- компоненты \(B_y\) на основной сетке, совмещённые с компонентами на подсетке.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{backward-coupling-used-values}
    \caption{Компоненты поля \(\mathbf{B}\), участвующие в обратном связывании}
    \label{fig:backward-coupling-used-values}
\end{figure}

Для получения значения компоненты \(\mathbf{B}\) на основной сетке усредняются значения компонент \(\mathbf{B}\) на подсетке, находящихся на том же ребре ячейки основной сетки, что и искомое значение. Рис.~\ref{fig:backward-coupling-bx-example} иллюстрирует это правило для компоненты \(B_x\). Заглавными буквами на нём обозначаются компоненты поля \(\mathbf{B}\) на основной сетке, строчными --- на подсетке. Вместе с \(B_x^1\) на одном ребре ячейки находится лишь одно значения поля на подсетке --- \(b_x^1\), поэтому в результате обратного связывания \(B_x^1=b_x^1\). На одном ребре с \(B_x^2\) находится уже 3 значения поля на подсетке: \(b_x^2\), \(b_x^3\), \(b_x^4\). Поэтому \(B_x^2=\frac13\left(b_x^2+b_x^3+b_x^4\right)\).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.55\textwidth]{backward-coupling-bx-example}
    \caption{Примеры обратного связывания с компонентой \(B_x\)}
    \label{fig:backward-coupling-bx-example}
\end{figure}

Приведём формулы, по которым производится обратное связывание. Будем следовать обозначениям граничных индексов, введённым в п.~\ref{subsec:forward-coupling}. В отличие от перевода индексов из подсетки в основную сетку, перевод из основной сетки в подсетку будет зависеть от того, для какой оси производится перевод. Если эта ось совпадает с осью, связанной с компонентой поля \(\mathbf{B}\), для которой производится обратное связывание, параметр \(\delta=1\), иначе \(\delta=0\). Пусть также \(\sigma=\left\lfloor\frac{p}{2}\right\rfloor\). С учётом этого, формула для перевода индексов из основной сетки в подсетку записывается следующим образом:
\begin{equation}\label{eq:mg-to-sg-index}
	a=
	\begin{cases}
		A-A_s & \text{, } A\in\left\{A_s,A_s+1\right\}\\
		a_f-A_f+A & \text{, } A\in\left\{A_f-1,A_f\right\}\\
		2+p\left(A-A_s-2\right)+\delta\sigma & \text{, иначе}
	\end{cases}
\end{equation}
Операция обратного связывания на гранях интерфейса, параллельных плоскости \(z=\mathrm{const}\), записывается следующим образом:
\begin{equation}\label{eq:front-bx-backward-coupling}
	\left.B_x\right|_{I+\frac12,J,K_s+1}=
	\begin{cases}
		\frac1p\sum\limits_{l=-\sigma}^{\sigma}\left.b_x\right|_{i+l+\frac12,j,k_s+1} & \text{, } I_s+2\leqslant I\leqslant I_f-2\\
		\left.b_x\right|_{i+\frac12,j,k_s+} & \text{, иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:front-by-backward-coupling}
	\left.B_y\right|_{I,J+\frac12,K_s+1}=
	\begin{cases}
		\frac1p\sum\limits_{l=-\sigma}^{\sigma}\left.b_y\right|_{i,j+l+\frac12,k_s+1} & \text{, } J_s+2\leqslant J\leqslant J_f-2\\
		\left.b_y\right|_{i,j+\frac12,k_s+1} & \text{, иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:back-bx-backward-coupling}
	\left.B_x\right|_{I+\frac12,J,K_f}=
	\begin{cases}
		\frac1p\sum\limits_{l=-\sigma}^{\sigma}\left.b_x\right|_{i+l+\frac12,j,k_f} & \text{, } I_s+2\leqslant I\leqslant I_f-2\\
		\left.b_x\right|_{i+\frac12,j,k_f} & \text{, иначе}
	\end{cases}
\end{equation}
\begin{equation}\label{eq:back-by-backward-coupling}
	\left.B_y\right|_{I,J+\frac12,K_f}=
	\begin{cases}
		\frac1p\sum\limits_{l=-\sigma}^{\sigma}\left.b_y\right|_{i,j+l+\frac12,k_f} & \text{, } J_s+2\leqslant J\leqslant J_f-2\\
		\left.b_y\right|_{i,j+\frac12,k_f} & \text{, иначе}
	\end{cases}
\end{equation}

В формулах~\ref{eq:front-bx-backward-coupling} и \ref{eq:back-bx-backward-coupling} \(i=\overline{1,i_f-1}\), \(j=\overline{1,j_f}\). В формулах~\ref{eq:front-by-backward-coupling} и \ref{eq:back-by-backward-coupling} \(i=\overline{1,i_f}\), \(j=\overline{1,j_f-1}\).

Стоит отметить, что при \(p=1\) каждой ячейке основной сетки соответствует ровно одна ячейка подсетки и их пространственное расположение совпадает. Отсюда следует, что операции связывания сводятся к копированию значения компоненты поля из ячейки в основной сетке в соответствующую ячейку подсетки и наоборот. За счёт этого FDTD на основной сетке и на подсетке при обновлении любой компоненты одного поля использует ровно такие же значения другого поля, как и при работе на целой сетке с крупными ячейками. Отсюда следует, что финальные распределения поля при использовании FDTD на целой сетке и при использовании FDTD вместе с NSG при \(p=1\) должны совпадать.

\subsection{Разбиение шага по времени в подсетке}
Обновление полей в подсетке с уменьшенным временным шагом представляет дополнительную возможность улучшения производительности метода FDTD. Для использования уменьшенного временного шага выделяется подобласть в основной сетке, обрамляющая подсетку, толщиной в несколько ячеек. Это область непосредственно примыкает к подсетке, и, как следствие, операции связывания производятся в ней. В качестве примера, на рис.~\ref{fig:fine-timestep-region} она закрашена серым цветом.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.45\textwidth]{fine-timestep-region}
    \caption{Область основной сетки, где используется уменьшенный временной шаг}
    \label{fig:fine-timestep-region}
\end{figure}

В этой области обновление полей производится с уменьшенным в \(q\) раз шагом по времени (в общем случае \(q\neq p\)). В связи с тем, что связывания производятся в пределах данной области, введение разбиения по времени не требует изменения общей схемы работы с подсеткой, приведённой в п.~\ref{sec:nsg-order} В остальной части основной сетки обновление производится с исходным временным шагом. Для того, чтобы в основной сетке и в подсетке время шло с одинаковой скоростью, необходимо производить \(q\) циклов обновления поля в подсетке на один цикл обновления поля в основной сетке. Рис.~\ref{fig:leapfrog-nsg} иллюстрирует схему проведения обновлений и их порядок для \(q=3\). Числа в скобках определяют порядок обновлений в рамках одного шага FDTD.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth]{leapfrog-nsg}
    \caption{Соотношение операций обновления с исходным и с уменьшенным временными шагами}
    \label{fig:leapfrog-nsg}
\end{figure}

