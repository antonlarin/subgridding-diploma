ROOT_DIR=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
BUILD_DIR=$(ROOT_DIR)/build
IMAGES_DIR=$(ROOT_DIR)/images

TEX_FILES=$(ROOT_DIR)/diploma.tex \
	$(ROOT_DIR)/mainpage.tex \
	$(ROOT_DIR)/content.tex \
	$(ROOT_DIR)/intro.tex \
	$(ROOT_DIR)/taskngoals.tex \
	$(ROOT_DIR)/methoddesc.tex \
	$(ROOT_DIR)/impl.tex \
	$(ROOT_DIR)/tests.tex \
	$(ROOT_DIR)/outro.tex

IMAGES_SRC_LIST=$(IMAGES_DIR)/two-axis-division.mp \
	$(IMAGES_DIR)/subgrid-embedding.mp \
	$(IMAGES_DIR)/e-mg-update-borders.mp \
	$(IMAGES_DIR)/b-mg-update-borders.mp \
	$(IMAGES_DIR)/e-sg-update-borders.mp \
	$(IMAGES_DIR)/b-sg-update-borders.mp \
	$(IMAGES_DIR)/forward-coupling-region.mp \
	$(IMAGES_DIR)/forward-coupling-used-values.mp \
	$(IMAGES_DIR)/forward-coupling-ex-example.mp \
	$(IMAGES_DIR)/forward-coupling-ey-example.mp \
	$(IMAGES_DIR)/backward-coupling-region.mp \
	$(IMAGES_DIR)/backward-coupling-used-values.mp \
	$(IMAGES_DIR)/backward-coupling-bx-example.mp \
	$(IMAGES_DIR)/yee-cell.mp \
	$(IMAGES_DIR)/leapfrog-fdtd.mp \
	$(IMAGES_DIR)/fine-timestep-region.mp \
	$(IMAGES_DIR)/leapfrog-nsg.mp

IMAGES_EPS_LIST=$(IMAGES_SRC_LIST:.mp=.eps)

XELATEX_FLAGS=-interaction=batchmode -output-directory=$(BUILD_DIR)

all: texfiles


images: $(IMAGES_EPS_LIST)
	cd $(IMAGES_DIR) && \
	epstopdf leapfrog-nsg.eps && \
	cd $(ROOT_DIR)


$(IMAGES_DIR)/%.eps : $(IMAGES_DIR)/%.mp
	cd $(IMAGES_DIR) && \
	mpost -interaction=batchmode $(notdir $<) && \
	cd $(ROOT_DIR)


texfiles: $(TEX_FILES) images bibliography
	xelatex $(XELATEX_FLAGS) diploma.tex
	xelatex $(XELATEX_FLAGS) diploma.tex


.PHONY: bibliography clean cleanmpx


bibliography: $(ROOT_DIR)/refs.bib
	xelatex $(XELATEX_FLAGS) diploma.tex
	biber $(BUILD_DIR)/diploma.bcf


clean: cleanmpx
	rm -rf $(IMAGES_DIR)/*.log $(IMAGES_DIR)/*.eps $(IMAGES_DIR)/*.pdf \
		$(BUILD_DIR)/*


cleanmpx:
	rm -f $(IMAGES_DIR)/*.mpx $(IMAGES_DIR)/*.tex $(IMAGES_DIR)/mptextmp.mp

